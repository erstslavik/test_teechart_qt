#include "openmdiqactioncommand.h"
#include <qmdisubwindow.h>
#include "fileparser.h"
#include "datatablemodel.h"
#include <qfiledialog.h>
#include <qmessagebox.h>
#include <qaxwidget.h>
#include <qvariant.h>
#include <qaxobject.h>
#include <qtableview.h>
#include <teechartview.h>
#include "runtimeerrorqstring.h"
#include <functional>
#include <qlabel.h>
#include <qscrollarea.h>
#include <qboxlayout.h>


void OpenMdiQActionCommand::doStrategy() NOEXCEPT
try{
	auto checkError=[&](bool check, RuntimeErrorWithPriority::RuntimeErrorPriority _priority, QString errorString)
	{
		if (check)
			throw RuntimeErrorWithPriority(_priority,errorString);
	};

	QString fileName = QFileDialog::getOpenFileName(nullptr, "Open file", "", "Txt files (*.txt)");
	if (fileName.isEmpty())
		return;
	FileParser fp;
	checkError(!fp.open(fileName), RuntimeErrorWithPriority::kCriticalPriority,
				"Can not open file "+fileName+":"+fp.getErrorString());
	QString errString;
	int okLines=0, errorLines=0;
	auto pDataStructTableModel=new DataTableModel();
	DataStruct readingDataStruct;
	std::vector<DataStruct> dataStructs;
	while (!fp.eof())
	{
		if (!fp.parseLine(readingDataStruct))
			++errorLines, errString+=fp.getErrorString()+" "+QString::number(errorLines+okLines)+'\n';
		else
			++okLines, dataStructs.push_back(readingDataStruct);
	}
	checkError(!okLines,  RuntimeErrorWithPriority::kCriticalPriority,
			  "File "+fileName+" has inpropriate format");

	pDataStructTableModel->insertRows(pDataStructTableModel->rowCount(),std::move(dataStructs));
	QTableView* tableView=new QTableView();
	tableView->setModel(pDataStructTableModel);
	_forCretingSubwindows.addSubWindow(tableView);
	TeeChartView* teeChartView=new TeeChartView();
	teeChartView->setModel(pDataStructTableModel);
	_forCretingSubwindows.addSubWindow(teeChartView);

	tableView->show();
	teeChartView->show();
	_forCretingSubwindows.tileSubWindows();

	checkError(!errString.isEmpty(),  RuntimeErrorWithPriority::kInfoPriority,
		"File " + fileName + " has "+QString::number(errorLines)+" errors:\n"+errString);
}
catch (const RuntimeErrorWithPriority& rException)
{
	switch (rException.priority())
	{
	case RuntimeErrorWithPriority::kCriticalPriority:
		QMessageBox::critical(nullptr, "Eror!", rException.whatQString());
		break;
	case RuntimeErrorWithPriority::kWarningPriority:
		QMessageBox::warning(nullptr, "Waning!", rException.whatQString());
		break;
	default:
		QScrollArea* scrollArea=new QScrollArea();
		QLabel* infoLabel=new QLabel(rException.whatQString());
		scrollArea->setWidget(infoLabel);
		auto warningSubwindow=_forCretingSubwindows.addSubWindow(scrollArea);
		scrollArea->show();
		_forCretingSubwindows.tileSubWindows();	
	}
}

