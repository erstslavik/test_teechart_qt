#include "mainwindow.h"
#include <QtWidgets/QApplication>

#include <qregexp.h>
#include <qdebug.h>

int main(int argc, char *argv[])
try{
	QApplication a(argc, argv);
	MainWindow w;
	w.show();
	return a.exec();
}
catch (...)
{
	return 0;
}
