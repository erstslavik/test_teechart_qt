#include "teechartview.h"
#include "datatablemodel.h"
#include "datastruct.h"
#include <qaxobject.h>
#include <memory>
#include <qscrollarea.h>
#include <qboxlayout.h>
#include <qscreen.h>
#include <qguiapplication.h>

#include <qdebug.h>

enum EBorderStyle { bsNone=0, bsSingle=1 };
enum ECandleStyle { csCandleStick=0, csCandleBar=1, csOpenClose=2, csLine=3 };
enum EChartScroll { pmNone=0, pmHorizontal=1, pmVertical=2, pmBoth=3 };
enum ESeriesClass { scCandle=11 };
enum EChartTheme  { ctBlackIsBack=10, ctgGrayscale=11 };
enum EMarginUnits { muPercent, muPixels};

class TeeChartFunctions
{
public:
	static const char* AddSeries;
	static const char* Series;
	static const char* asCandle;
	static const char* AddCandle;
	static const char* Legend;
	static const char* Visible;
	static const char* Scroll;
	static const char* Enabled;
	static const char* CandleStyle;
	static const char* BorderStyle;
	static const char* SetTheme;
	static const char* Aspect;
	static const char* View3D;
	static const char* Panel;
	static const char* MarginLeft;
	static const char* MarginRight;
	static const char* MarginUnits;
	static const char* Axis;
	static const char* Bottom;
	static const char* Labels;
	static const char* MultiLine;
	static const char* DateTimeFormat;
};

TeeChartView::TeeChartView(QWidget * parent): QAbstractItemView(parent)
{
	//Hierarchy: scrollArea -> nWidget -> nLayout -> _teeChartWidget

	_teeChartWidget=new QAxWidget("{CC0427C7-2124-4770-8847-F9EF4C50CDC2}");
	// For scrolling
	scrollArea=new QScrollArea(this);
	// Without this widget, which, de facto is interlayer, chart will be too frequently redrawing, so the program freezes
	nWidget=new QWidget();
	// For adjusting for all nWidget space
	QHBoxLayout* nLayout=new QHBoxLayout();
	nLayout->addWidget(_teeChartWidget);
	nWidget->setLayout(nLayout);
	scrollArea->setWidgetResizable(false);
	scrollArea->setWidget(nWidget);
}

// Gets data from @model and create proper chart.
void TeeChartView::setModel(QAbstractItemModel *model)
{
	QAbstractItemView::setModel(model);
	DataTableModel* dtModelUsed=qobject_cast<DataTableModel*>(this->model());
	if (!dtModelUsed)
		return;

	static const QColor usedColor("green");
	QVariantList params;
	
	//_teeChartWidget take this size.
	nWidget->resize(dtModelUsed->rowCount()*3,QGuiApplication::primaryScreen()->availableSize().height()*3/4);
	

	params.push_back(QVariant(scCandle));
	_teeChartWidget->dynamicCall(TeeChartFunctions::AddSeries, params);
	params.clear();

	//Get first series
	params.push_back(QVariant(0));
	std::unique_ptr<QAxObject> resultSeries0(_teeChartWidget->querySubObject(TeeChartFunctions::Series, params));
	params.clear();

	std::unique_ptr<QAxObject> myCandle(resultSeries0->querySubObject(TeeChartFunctions::asCandle, params));
	params.clear();

	params.push_back(QVariant(csCandleBar));
	myCandle->dynamicCall(TeeChartFunctions::CandleStyle, params);
	params.clear();

	for (int row=0; row<dtModelUsed->rowCount(); ++row)
	{
		params=static_cast<QVariantList>(dtModelUsed->getRow(row, Qt::DisplayRole));
		params.push_back("");
		params.push_back(usedColor);
		myCandle->dynamicCall(TeeChartFunctions::AddCandle, params);
		params.clear();
	}

	// Make legend invisible.
	{
		std::unique_ptr<QAxObject> legendAxObject(_teeChartWidget->querySubObject(TeeChartFunctions::Legend, params));
		params.clear();

		params.push_back(QVariant(false));
		legendAxObject->dynamicCall(TeeChartFunctions::Visible, params);
		params.clear();
	}

	// Enable scrolling.
	{
		std::unique_ptr<QAxObject> scrollAxObject(_teeChartWidget->querySubObject(TeeChartFunctions::Scroll, params));
		params.clear();

		params.push_back(QVariant(pmBoth));
		scrollAxObject->dynamicCall(TeeChartFunctions::Enabled, params);
		params.clear();
	}

	// Make View3D off.
	{
		std::unique_ptr<QAxObject> aspectAxObject(_teeChartWidget->querySubObject(TeeChartFunctions::Aspect, params));
		params.clear();

		params.push_back(QVariant(false));
		aspectAxObject->dynamicCall(TeeChartFunctions::View3D, params);
		params.clear();
	}

	// Set left and right margins to 15px.
	{
		std::unique_ptr<QAxObject> panelAxObject(_teeChartWidget->querySubObject(TeeChartFunctions::Panel, params));
		params.clear();

		params.push_back(QVariant(muPixels));
		panelAxObject->dynamicCall(TeeChartFunctions::MarginUnits , params);
		params.clear();

		params.push_back(QVariant(15));
		panelAxObject->dynamicCall(TeeChartFunctions::MarginLeft, params);
		params.clear();

		params.push_back(QVariant(15));
		panelAxObject->dynamicCall(TeeChartFunctions::MarginRight, params);
		params.clear();
	}
	
	//Design axis labels
	{
		std::unique_ptr<QAxObject> axesAxObject(_teeChartWidget->querySubObject(TeeChartFunctions::Axis, params));
		std::unique_ptr<QAxObject> axisBottomAxObject(axesAxObject->querySubObject(TeeChartFunctions::Bottom, params));
		std::unique_ptr<QAxObject> axisLabelsAxObject(axisBottomAxObject->querySubObject(TeeChartFunctions::Labels, params));
		params.clear();

		params.push_back(QVariant(true));
		axisLabelsAxObject->dynamicCall(TeeChartFunctions::MultiLine, params);
		params.clear();

		params.push_back(QVariant("d mmm yyyy"));
		axisLabelsAxObject->dynamicCall(TeeChartFunctions::DateTimeFormat, params);
		params.clear();
	}
}

void TeeChartView::updateGeometries()
{
	scrollArea->resize(size());
}

QRect TeeChartView::visualRect(const QModelIndex &index) const
{
	return _teeChartWidget->rect();
}

void TeeChartView::scrollTo(const QModelIndex &index, ScrollHint hint)
{
}

QModelIndex TeeChartView::indexAt(const QPoint &point) const
{ 
	return QModelIndex();
}

QModelIndex TeeChartView::moveCursor(CursorAction cursorAction,
                                   Qt::KeyboardModifiers modifiers)
{
	return QModelIndex();
}

int TeeChartView::horizontalOffset() const
{
	return 0;
}

int TeeChartView::verticalOffset() const
{
	return 0;
}

bool TeeChartView::isIndexHidden(const QModelIndex &index) const
{
	return true;
}

void TeeChartView::setSelection(const QRect &rect, QItemSelectionModel::SelectionFlags command)
{
}

QRegion TeeChartView::visualRegionForSelection(const QItemSelection &selection) const
{
	return QRegion();
}

const char* TeeChartFunctions::AddSeries="AddSeries(ESeriesClass)";
const char* TeeChartFunctions::Series="Series(int)";
const char* TeeChartFunctions::asCandle="asCandle()";
const char* TeeChartFunctions::AddCandle="AddCandle(double, double, double, double, double, QString, QColor)";
const char* TeeChartFunctions::Legend="Legend()";
const char* TeeChartFunctions::Visible="SetVisible(bool)";
const char* TeeChartFunctions::Scroll="Scroll()";
const char* TeeChartFunctions::Enabled="SetEnable(EChartScroll)";
const char* TeeChartFunctions::CandleStyle="SetCandleStyle(ECandleStyle)";
const char* TeeChartFunctions::BorderStyle="SetBorderStyle(EBorderStyle)";
const char* TeeChartFunctions::SetTheme="SetTheme(EChartTheme, EColorPalette)";
const char* TeeChartFunctions::Aspect="Aspect()";
const char* TeeChartFunctions::View3D="SetView3D(bool)";
const char* TeeChartFunctions::Panel="Panel()";
const char* TeeChartFunctions::MarginLeft="SetMarginLeft(int)";
const char* TeeChartFunctions::MarginRight="SetMarginRight(int)";
const char* TeeChartFunctions::MarginUnits="SetMarginUnits(EMarginUnits)";
const char* TeeChartFunctions::Axis="Axis()";
const char* TeeChartFunctions::Bottom="Bottom()";
const char* TeeChartFunctions::Labels="Labels()";
const char* TeeChartFunctions::MultiLine="SetMultiLine(bool)";
const char* TeeChartFunctions::DateTimeFormat="SetDateTimeFormat(const QString&)";