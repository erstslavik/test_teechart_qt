#include "fileparser.h"
#include <qregexp.h>
#include <functional>
#include "runtimeerrorqstring.h"

/** 
  * Process one line of file.
  *  
  * @param[out] curLineData   reference to dataStruct with current line data.
  *							  If error occured, while reading line, curLineData will be invalid
  *							  but next line can be readed if not eof.
  * @ret FALSE - if error occured, otherwise TRUE
  */
bool FileParser::parseLine(DataStruct& curLineData)
try{
	auto checkError=[&](bool check, QString errorString)
	{
		if (check)
			throw RuntimeErrorQString(errorString);
	};

	//Not rewrite already stored in _errorString info about file opening or reading (not formating error)
	checkError(_file.error()!=QFile::NoError, QString());

	//ignore previous formating error
	_errorString = QString();
	QString line = _file.readLine();
	checkError(_file.error()!=QFile::NoError, _file.errorString());
	checkError(rightFormat.indexIn(line)==-1, "Bad format: error at line");
	QStringList matchedList = rightFormat.capturedTexts();
	int yearReaded=matchedList[kYearInRegex].toInt();
	int monthReaded=matchedList[kMonthInRegex].toInt();
	int dayReaded=matchedList[kDayInRegex].toInt();
	checkError(!curLineData.setDate(QDate(yearReaded,monthReaded,dayReaded)), "Bad date: error at line");
	checkError(!curLineData.setOpenPrice(matchedList[kOpenPriceInRegex]), "Bad open price: error at line");
	checkError(!curLineData.setClosePrice(matchedList[kClosePriceInRegex]), "Bad close price: error at line");
	checkError(!curLineData.setHighPrice(matchedList[kHighPriceInRegex]), "Bad hight price: error at line");
	checkError(!curLineData.setLowPrice(matchedList[kLowPriceInRegex]), "Bad low price: error at line");

	curLineData.isValid=true; 
	return true;
}
catch(const RuntimeErrorQString& rException)
{
	if (!rException.whatQString().isEmpty())
		this->_errorString=rException.whatQString();
	curLineData.isValid=false;
	return false;
}

bool FileParser::open(QString fileToOpen)
{
	_file.unsetError();
	_errorString = QString();
	_file.setFileName(fileToOpen);
	if (!_file.open(QFile::ReadOnly|QFile::Text))
	{
		_errorString = _file.errorString();
		return false;
	}

	return true;
}

const QRegExp FileParser::rightFormat=QRegExp("^(\\d{4})(\\d{2})(\\d{2})\\s*,\\s*(\\d+.\\d+)\\s*,\\s*(\\d+.\\d+)\\s*,\\s*(\\d+.\\d+)\\s*,\\s*(\\d+.\\d+)\\s*,?\\s*$");
