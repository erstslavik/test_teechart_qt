#pragma once
#include "abstractqactioncommand.h"
#include <qmdiarea.h>

/**
  * Strategy implementation of strategy design pattern.
  * Implement strategy, that parse opened file, show summary and both table and chart subwindows.
  */
class OpenMdiQActionCommand :
	public AbstractQActionCommand
{
	Q_OBJECT
protected:
	QMdiArea& _forCretingSubwindows;

public:
	explicit OpenMdiQActionCommand(QMdiArea& forCretingSubwindows, QObject* parent=nullptr): AbstractQActionCommand(parent),
																_forCretingSubwindows(forCretingSubwindows)
		{}

    OpenMdiQActionCommand(QMdiArea& forCretingSubwindows, const QString &text, QObject* parent=nullptr): AbstractQActionCommand(text, parent),
																_forCretingSubwindows(forCretingSubwindows)		
		{ }

    OpenMdiQActionCommand(QMdiArea& forCretingSubwindows, const QIcon &icon, const QString &text, QObject* parent=nullptr): 
																AbstractQActionCommand(icon, text,parent),
																_forCretingSubwindows(forCretingSubwindows)
		{ }

public slots:
	virtual  void doStrategy() override;
};

