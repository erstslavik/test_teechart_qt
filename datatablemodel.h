#pragma once
#include <qabstractitemmodel.h>
#include <vector>
#include "datastruct.h"

/**
  * Model implementation class for Qt's MVC pattern.
  * Row can be inserted or deleted, but column not.
  * Data isn't editable through delegate.
  * @see QAbstractTableModel.
  */
class DataTableModel : 	public QAbstractTableModel
{
	Q_OBJECT
private:
	std::vector<DataStruct> _dataModel;
	enum ToIntValueRowTranslate { kDateRow, kOpenPriceRow, kHighPriceRow, kLowPriceRow, kClosePriceRow };
	static const int _columnCount=5;

public:
	virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override final;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override final;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override final;
	virtual QVariant headerData(int section, Qt::Orientation orientation,
                                int role = Qt::DisplayRole) const override final;
	bool insertRows(int row,std::vector<DataStruct>&& dataToInsert,
					const QModelIndex &parent = QModelIndex());
	bool insertRows(int row,const std::vector<DataStruct>& dataToInsert,
					const QModelIndex &parent = QModelIndex());
	bool insertRow(int row, const DataStruct& dataToInsert,
					const QModelIndex &parent = QModelIndex());
    virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override final;

	/**
	  * Used in TeeChartView for easy adding candle
	  * @ret DataStruct ad @row.
	  * If @role isn't Qt::DisplayRole or isn't in valid range return invalid DataStruct.
	  * @see TeeChartView
	  */
	inline DataStruct getRow(int row, int role=Qt::DisplayRole) const
		{ 	return (row>=rowCount() || Qt::DisplayRole!=role) ?  DataStruct() : _dataModel[row]; }
};

