#pragma once
#include <qaction.h>
#include <constants.h>

// Interface in strategy design pattern
class AbstractQActionCommand :
	public QAction
{
	Q_OBJECT
private:
	inline void connectTriggeredToDoStrategy() NOEXCEPT
	{ connect(this, &AbstractQActionCommand::triggered, this, &AbstractQActionCommand::doStrategy); } 

public:
	explicit AbstractQActionCommand(QObject* parent=nullptr): QAction(parent)
		{ connectTriggeredToDoStrategy(); }

    AbstractQActionCommand(const QString &text, QObject* parent=nullptr): QAction(text, parent)
		{ connectTriggeredToDoStrategy (); }

	AbstractQActionCommand(const QIcon &icon, const QString &text, QObject* parent=nullptr): QAction( icon, text, parent)
		{ connectTriggeredToDoStrategy (); }

public slots:
	virtual  void doStrategy() NOEXCEPT =0;
};

