#include "datatablemodel.h"

int DataTableModel::rowCount(const QModelIndex &parent) const
	{  return _dataModel.size(); }

int DataTableModel::columnCount(const QModelIndex &parent) const
	{ return _columnCount; }

QVariant DataTableModel::data(const QModelIndex &index, int role ) const
{ 
	if (role!=Qt::DisplayRole || !index.isValid() || index.row()>=rowCount())
		return QVariant();
	auto dataStructVal=_dataModel[index.row()];
	switch (index.column())
	{
	case kDateRow:
		return QVariant(dataStructVal.getDate());
		break;
	case kOpenPriceRow:
		return QVariant(dataStructVal.getOpenPrice());
		break;
	case kHighPriceRow:
		return QVariant(dataStructVal.getHighPrice());
		break;
	case kLowPriceRow:
		return QVariant(dataStructVal.getLowPrice());
		break;
	case kClosePriceRow:
		return QVariant(dataStructVal.getClosePrice());
		break;
	default:
		return QVariant();
	}
}

QVariant DataTableModel::headerData(int section, Qt::Orientation orientation,
                                int role) const
{
	if (role!=Qt::DisplayRole)
		return QVariant();
	if (Qt::Vertical==orientation)
		return QVariant(section);

	//Doesn't use vector due to possibility changing in values of ToIntValueRowTranslate enum
	
	static const QString dateHeaderString="Date";
	static const QString openPriceHeaderString="Open price";
	static const QString highPriceHeaderString="High price";
	static const QString lowPriceHeaderString="Low price";
	static const QString closePriceHeaderString="Close price";
	switch (section)
	{
	case kDateRow:
		return QVariant(dateHeaderString);
		break;
	case kOpenPriceRow:
		return QVariant(openPriceHeaderString);
		break;
	case kHighPriceRow:
		return QVariant(highPriceHeaderString);
		break;
	case kLowPriceRow:
		return QVariant(lowPriceHeaderString);
		break;
	case kClosePriceRow:
		return QVariant(closePriceHeaderString);
		break;
	default:
		return QVariant();
	}
}


bool DataTableModel::insertRow(int row, const DataStruct& dataToInsert,
								const QModelIndex &parent)
{
	std::vector<DataStruct> dataStructs(1,dataToInsert);
	return insertRows(row, std::move(dataStructs), parent);
}

bool DataTableModel::insertRows(int row,std::vector<DataStruct>&& dataToInsert,
					const QModelIndex &parent)
{
	if (row=rowCount() || parent.isValid())
		return false;
	for (DataStruct dataStructsIter: dataToInsert)
		if (!dataStructsIter.isValid)
			return false;
	beginInsertRows(parent, row, row+dataToInsert.size()-1);
	_dataModel.insert(_dataModel.begin(), std::make_move_iterator(dataToInsert.begin()),
					  std::make_move_iterator(dataToInsert.end()));
	endInsertRows();
	return true;
}

bool DataTableModel::insertRows(int row,const std::vector<DataStruct>& dataToInsert,
					const QModelIndex &parent)
{
	if (row=rowCount() || parent.isValid())
		return false;
	for (DataStruct dataStructsIter: dataToInsert)
		if (!dataStructsIter.isValid)
			return false;
	beginInsertRows(parent, row, row+dataToInsert.size()-1);
	_dataModel.insert(_dataModel.begin(), dataToInsert.cbegin(), dataToInsert.cend());
	endInsertRows();
	return true;
}

bool DataTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
	if (row>=rowCount() || parent.isValid())
		return false;
	beginRemoveRows(parent, row, row+count-1);
	_dataModel.erase(_dataModel.begin()+row,_dataModel.begin()+row+count);
	endRemoveRows();
	return true;
}
