#pragma once
#include <stdexcept>
#include <qstring.h>

class RuntimeErrorQString :
	public std::runtime_error
{
public:
	explicit RuntimeErrorQString(const QString& _Message): std::runtime_error(_Message.toStdString())
	{
	}
	using runtime_error::runtime_error;

	inline QString whatQString() const
	{
		return what();
	}
};

class RuntimeErrorWithPriority :
	public RuntimeErrorQString
{
public:
	enum RuntimeErrorPriority { kInfoPriority, kWarningPriority, kCriticalPriority };

protected:
	RuntimeErrorPriority _priority;

public:
	RuntimeErrorWithPriority(RuntimeErrorPriority vPriority, const QString& _Message): RuntimeErrorQString(_Message), _priority(vPriority)
		{}
	RuntimeErrorWithPriority(RuntimeErrorPriority vPriority, const std::string& _Message): RuntimeErrorQString(_Message.c_str()), _priority(vPriority)
		{}
	RuntimeErrorWithPriority(RuntimeErrorPriority vPriority, const char * _Message): RuntimeErrorQString(_Message), _priority(vPriority)
		{}

	inline RuntimeErrorPriority priority() const
	{
		return _priority;
	}
};
