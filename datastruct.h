#pragma once
#include "constants.h"
#include <qstring.h>
#include <qdatetime.h>
#include <qvariant.h>
#include <qlist.h>

/**
  * Set/get interface presented, 
  * so openPrice/highPrice/lowPrice/closePriceonly can take only correct non-negative real value.
  * Invalid data indicated by @isValid.
  * Default constructor create invalid dataStruct.
  */
struct DataStruct
{
private:
	QDate _date;
	QString _openPrice;
	QString _highPrice;
	QString _lowPrice;
	QString _closePrice;

	// @ret  True is val is non-negative double, else FALSE
	bool isVerified(QString val) NOEXCEPT
	{
		bool _isDouble=false;
		double doubleVal=val.toDouble(&_isDouble);
		if (!_isDouble || doubleVal<0)
			return false;
		return true;
	}

public:
	bool isValid;


	DataStruct(): isValid(false), _date(QDate::currentDate()), _openPrice("0"), _highPrice("0"), _lowPrice("0"), _closePrice("0")
		{}

	inline QDate getDate() const NOEXCEPT
		{ return _date; }
	inline bool setDate(QDate date) NOEXCEPT
		{ return date.isValid()?_date=date,true:false;	}

	inline QString getOpenPrice() const NOEXCEPT
		{ return _openPrice; }
	inline bool setOpenPrice(QString openPrice) NOEXCEPT
		{ return isVerified(openPrice)?_openPrice=openPrice,true:false; }

	inline QString getHighPrice() const NOEXCEPT
		{ return _highPrice; }
	inline bool setHighPrice(QString highPrice) NOEXCEPT
		{ return isVerified(highPrice)?_highPrice=highPrice,true:false; }

	inline QString getLowPrice() const NOEXCEPT
		{ return _lowPrice; }
	inline bool setLowPrice(QString lowPrice) NOEXCEPT
		{ return isVerified(lowPrice)?_lowPrice=lowPrice,true:false; }

	inline QString getClosePrice() const NOEXCEPT
		{ return _closePrice; }
	inline bool setClosePrice(QString closePrice) NOEXCEPT
		{ return isVerified(closePrice)?_closePrice=closePrice,true:false; }

	/**
	  * Used for easy addint to TeeChart's Candle.
	  * Date is in VB format, needed by TeeChart.
	  * @ret VariantList, which contains QVariantList with Date, OpenPrice, HighPrice, LowPrice and ClosePrice.
	  */
	operator QVariantList() const NOEXCEPT
	{
		QVariantList variantList;
		if (!isValid)
			return variantList;
		//Convert Date to Vb double value.
		variantList.push_back(QVariant(static_cast<double>(QDate(1899,12,31).daysTo(getDate()))));
		variantList.push_back(QVariant(getOpenPrice()));
		variantList.push_back(QVariant(getHighPrice()));
		variantList.push_back(QVariant(getLowPrice()));
		variantList.push_back(QVariant(getClosePrice()));
		return variantList;
	}
};

