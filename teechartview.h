#pragma once
#include <qabstractitemview.h>
#include <qaxwidget.h>
#include <qscrollarea.h>

/**
  * Simple view class, for creating chart using TeeChart,
  * based on model.
  * Class not enable editing data, also as delegate class,
  * but it's enough for this project.
  * Only @setModel(QAbstractItemModel*), @updateGeometries() and @TeeChartView(QWidget*) 
  * real tasks, other just implement pure virtual function.
  * @see QAbstractItemView 
  */
class TeeChartView :
	public QAbstractItemView
{
	Q_OBJECT

public:
	QAxWidget* _teeChartWidget;
	QScrollArea* scrollArea;
	QWidget* nWidget;
public:
	explicit TeeChartView(QWidget * parent = 0);

	// Gets data from @model and create proper chart.
	virtual void setModel(QAbstractItemModel *model) override;

	virtual QRect visualRect(const QModelIndex &index) const override;
    virtual void scrollTo(const QModelIndex &index, ScrollHint hint = EnsureVisible) override;
    virtual QModelIndex indexAt(const QPoint &point) const override;
protected slots:
	virtual void updateGeometries() override;

protected:
	virtual QModelIndex moveCursor(CursorAction cursorAction,
                                   Qt::KeyboardModifiers modifiers) override;
    virtual int horizontalOffset() const override;
    virtual int verticalOffset() const override;
    virtual bool isIndexHidden(const QModelIndex &index) const override;
    virtual void setSelection(const QRect &rect, QItemSelectionModel::SelectionFlags command) override;
    virtual QRegion visualRegionForSelection(const QItemSelection &selection) const override;
};

