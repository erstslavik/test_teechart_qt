#include "mainwindow.h"
#include <qmdiarea.h>
#include "openmdiqactioncommand.h"
#include <qmenubar.h>

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
{
	auto center=new QMdiArea();
	this->setCentralWidget(center);
	OpenMdiQActionCommand* command=new OpenMdiQActionCommand(*center,"Open", this);
	this->menuBar()->addAction(command);
}
