#pragma once
#include "constants.h"
#include "datastruct.h"
#include <qfile.h>

/**
  * Class for parsing file in format 
  * Date,OpenPrice,HighPrice,LowPrice,ClosePrice\n
  * e. q. 
  * 19950103,131.97500000,133.02500000,131.90000000,132.72500000
  * 
  * Parsing is done in way one function call processing only one line of file.
  * Class doesn't throw exception while parsing due to perfomance reason, 
  * so to unified exception doesn't throwing by all function, 
  * so not to throw by constructor I decide implement open(QString) 
  * and not propriate constructor. 
  * @see parseLine()
  * @see open(QString)
  */
class FileParser
{
private:
	QFile _file;
	QString _errorString;

	static const QRegExp rightFormat;
	enum GetRowFromRegex {	kYearInRegex=1, kMonthInRegex, kDayInRegex, kOpenPriceInRegex, 
		kHighPriceInRegex, kLowPriceInRegex, kClosePriceInRegex };
public:
	/** 
	  * Process one line of file.
	  *  
	  * @param[out] curLineData   reference to dataStruct with current line data.
	  *							  If error occured, while reading line, curLineData will be invalid
	  *							  but next line can be readed if not eof.
	  * @ret FALSE - if error occured, otherwise TRUE
	  */
	bool parseLine(DataStruct& curLineData) NOEXCEPT;

	bool open(QString fileToOpen) NOEXCEPT;
	
	bool eof() const NOEXCEPT
		{ return _file.atEnd(); }

	// @ret Error message is there is an error, at if all is ok (including eof) - empty string
	QString getErrorString() const NOEXCEPT
		{ return _errorString; }

};

